//
//  NYCSBaseServices.swift
//  20190416-SudhaPolicePatil-NYCSchools
//
//  Created by Sudha P on 16/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import Foundation

/// Protocol created for Serializable.
protocol Serializable: Codable {
    func serialize() -> Data?
}

// MARK: - Added Extension to the Serializable.
extension Serializable {
    func serialize() -> Data? {
        let encoder = JSONEncoder()
        return try? encoder.encode(self)
    }
}

/// Enum created for Service call Type.
///
/// - GET: Used to make GET Type.
/// - POST: Used to make POST Type.
enum ServiceTypeEnum: String {
    case GET = "GET"
    case POST = "POST"
}

/// Base Service Call created to make service call.
class NYCSBaseServices: NSObject {
    
    /// Variable created for to check the Service Type GET or POST.
    var serviceType: ServiceTypeEnum!
    /// Variable user to store base url for making service call.
    var serviceURL:String = ""
    
    /// Convenience initializer.
    ///
    /// - Parameters:
    ///   - serviceType: Contains Service Type GET or POST.
    ///   - serviceURL: Contains base url.
    ///   - requestData: Contain request data if any.
    convenience init(serviceType:ServiceTypeEnum, serviceURL: String, requestData: Data? = nil) {
        self.init()
        self.serviceType = serviceType
        self.serviceURL = serviceURL
    }
    
    /// Method used to make service call.
    ///
    /// - Parameters:
    ///   - onSuccess: It's callBack on service success method. It contains the response model.
    ///   - onFailure: It's  callBack for service failure if there is any failure on service call.
    func start(onSuccess: @escaping (Data) -> Void, onFailure: @escaping (_ error: Error) -> Void) {
        let url:String = self.serviceURL
        var request: URLRequest = URLRequest(url: URL(string: url)!)
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60
        
        let session :Foundation.URLSession = URLSession(configuration: configuration, delegate: self as? URLSessionDelegate, delegateQueue: OperationQueue.main)
        request.setValue("application/json", forHTTPHeaderField: "Content-type")
        request.httpMethod = serviceType.rawValue
        let dataTask = session.dataTask(with: request, completionHandler: {
            data, response, error -> Void in
            if let error = error {
                print("error \(String(describing: error))")
                if NYCServiceErrorHadler.sharedInstance.showServiceError {
                    if error._code == NSURLErrorTimedOut {
                        NYCServiceErrorHadler.sharedInstance.serviceErrorDelegate?.showServiceError(error:ServiceErrorEnum.serviceTimeout)
                    }else if error._code == NSURLErrorNotConnectedToInternet {
                        NYCServiceErrorHadler.sharedInstance.serviceErrorDelegate?.showServiceError(error:ServiceErrorEnum.internetOffline)
                    }else{
                        NYCServiceErrorHadler.sharedInstance.serviceErrorDelegate?.showServiceError(error:ServiceErrorEnum.serviceFailure)
                    }
                }
                onFailure(error)
            }else{
                onSuccess(data! as Data)
            }
        })
        dataTask.resume()
    }
}
