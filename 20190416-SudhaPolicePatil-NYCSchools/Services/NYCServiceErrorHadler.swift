//
//  NYCServiceErrorHadler.swift
//  20190416-SudhaPolicePatil-NYCSchools
//
//  Created by Sudha P on 17/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import Foundation


/// Enum created for Error.
///
/// - serviceTimeout: Used to check for Service TimeOut
/// - invalidModel: Used to check for Invalid Model
/// - serviceFailure: Used to check for Service Failure
/// - internetOffline: Used to check for Internet Issue.
public enum ServiceErrorEnum: Error {
    case serviceTimeout
    case invalidModel
    case serviceFailure
    case internetOffline
}

/// Delegate method created to pass service error to the adopted class.
public protocol ServiceErrorAlertDelegate:class {
    func showServiceError(error:ServiceErrorEnum)
}

/// Class used to handle Service Error.
class NYCServiceErrorHadler: NSObject {
    
    static let sharedInstance = NYCServiceErrorHadler()
    weak var serviceErrorDelegate:ServiceErrorAlertDelegate?
    var showServiceError = true
    private override init() {
        // initialization not required
    }
}
