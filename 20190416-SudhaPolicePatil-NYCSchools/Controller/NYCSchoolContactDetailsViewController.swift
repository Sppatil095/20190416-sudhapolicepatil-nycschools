//
//  NYCSchoolContactDetailsViewController.swift
//  20190416-SudhaPolicePatil-NYCSchools
//
//  Created by Sudha P on 17/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import UIKit
import MapKit
/**
 The purpose of the `NYCSchoolContactDetailsViewController` view controller is used to show Contact and SAT Score of the school.
 
 - This will appear when user opens the app.
 
 There's a matching scene in the *Main.storyboard* file, Go to Interface Builder for details.
 
 The `NYCSchoolContactDetailsViewController` class is a subclass of the `UIViewController`.
 */

class NYCSchoolContactDetailsViewController: UIViewController {
    
    /// Outlet created for table view. Which is used to show the contact details of the school.
    @IBOutlet weak var contactDetailstableView: UITableView!
    /// Outlet created to show the Map on the view.
    @IBOutlet weak var mapKit: MKMapView!
    /// Property created to store NYCSchoolServiceResponseModel. Which selected school data.
    var nycSchoolDetailsModel: NYCSchoolServiceResponseModel?
    /// Property created as array of NYCSchoolSATScoreResponseModel. Which contains the list of SAT Score.
    var nycSchoolSATScoreResponseModel: [NYCSchoolSATScoreResponseModel] = []
    /// Property used to check user taps on contact details or SAT Score.
    var selectedCellType: SelectedTypeEnum?
    /// Latitude region set.
    let latDelta:CLLocationDegrees = 0.001
    /// Latitude region set.
    let lonDelta:CLLocationDegrees = 0.001
    /// Property created to show the loader while making service call.
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    /// Called after the controller's view is loaded into memory.
    var satScoreModel: NYCSchoolSATScoreResponseModel?
    /// Property used to store array of SchoolDetails.
    var schoolDetailsModel:[SchoolDetails] = [SchoolDetails]()
    /// Property used to store array of SchoolContactDetails.
    var schoolContactModel:[SchoolDetails] = [SchoolDetails]()
    
    /// Called after the controller's view is loaded into memory.
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initialSetUp()
        self.title = selectedCellType == SelectedTypeEnum.ContactDetails ? AppConstants.contactD : AppConstants.SATS
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    /// Method used to make initial setup for the controller. It contains loading nib file to the tableview and tableview setup.
    func initialSetUp(){
        let nibNameForSchoolCell = UINib(nibName: String(describing: SchoolInfoTableViewCell.self) , bundle: nil)
        contactDetailstableView.register(nibNameForSchoolCell, forCellReuseIdentifier: String(describing: SchoolInfoTableViewCell.self))
        contactDetailstableView.dataSource = self
        contactDetailstableView.delegate = self
        
        contactDetailstableView.estimatedRowHeight = 100
        self.activityIndicator.isHidden = true
        mapKit.setVisibleMapRect(mapKit.visibleMapRect, edgePadding: UIEdgeInsets(top: 200, left: 0, bottom: 0, right: 0), animated: true)
        mapKit.showsUserLocation = false
        mapKit.delegate = self
        
        self.setStoreLocation()
        self.createSchoolContactDetailsModel()
        if selectedCellType == SelectedTypeEnum.SatScore {
            self.contactDetailstableView.isHidden = true
            self.mapKit.isHidden = true
            self.activityIndicator.startAnimating()
            getSATScoreDetails()
        }
    }
    
    /// Method used to get the SAT Score.
    func getSATScoreDetails(){
        self.activityIndicator.isHidden = false
        NYCSServiceHandler.getSATScoreData(onSuccess: { (satScoreResponseModel) in
            self.nycSchoolSATScoreResponseModel = satScoreResponseModel
            self.contactDetailstableView.isHidden = false
            self.getIndexOfTheElement()
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
            DispatchQueue.main.async {
                self.contactDetailstableView.reloadData()
            }
        }, onFailure: { (error) in
            print(error ?? "")
            self.activityIndicator.stopAnimating()
            self.showErrorAlert()
        })
    }
    
    /// Methos used to show the error alert in case of failure in Service cal.
    func showErrorAlert() {
        let alert = UIAlertController(title: "TimeOut", message: "Something went wrong. Please check your internet connection.",         preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok",
                                      style: UIAlertActionStyle.default,
                                      handler: {(_: UIAlertAction!) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    /// Method used to get the index from the SAT Score based on dbn number.
    func getIndexOfTheElement(){
        self.satScoreModel = nycSchoolSATScoreResponseModel.filter { return $0.dbn == nycSchoolDetailsModel?.dbn}.first
        self.createSchoolDetailsModel()
    }
    
    /// Method used to created model for School details.
    func createSchoolDetailsModel(){
        if let satScoreModel = self.satScoreModel {
            self.schoolDetailsModel.append(SchoolDetails(headerValue:AppConstants.dbn, subValue:satScoreModel.dbn ?? "", image: ""))
            self.schoolDetailsModel.append(SchoolDetails(headerValue:AppConstants.num_of_sat_test_takers, subValue:satScoreModel.num_of_sat_test_takers ?? "", image: ""))
            self.schoolDetailsModel.append(SchoolDetails(headerValue:AppConstants.sat_critical_reading_avg_score, subValue:satScoreModel.sat_critical_reading_avg_score ?? "", image: ""))
            self.schoolDetailsModel.append(SchoolDetails(headerValue:AppConstants.sat_math_avg_score, subValue:satScoreModel.sat_math_avg_score ?? "", image: ""))
            self.schoolDetailsModel.append(SchoolDetails(headerValue:AppConstants.sat_writing_avg_score, subValue:satScoreModel.sat_writing_avg_score ?? "", image: ""))
        }
    }
    
    /// Method used to created model for School contact details.
    func createSchoolContactDetailsModel(){
        self.schoolContactModel.append(SchoolDetails(headerValue: AppConstants.phoneNumber, subValue: nycSchoolDetailsModel?.phone_number ?? "", image: "NS_User_Profile"))
        self.schoolContactModel.append(SchoolDetails(headerValue: AppConstants.schoolEmail, subValue: nycSchoolDetailsModel?.school_email ?? "", image: "NS_Email_Image"))
        self.schoolContactModel.append(SchoolDetails(headerValue: AppConstants.website, subValue: nycSchoolDetailsModel?.website ?? "", image: "NS_Wesite_Image"))
        self.schoolContactModel.append(SchoolDetails(headerValue: AppConstants.faxNumber, subValue: nycSchoolDetailsModel?.fax_number ?? "", image: "NS_Fax_Image"))
        self.schoolContactModel.append(SchoolDetails(headerValue: AppConstants.address, subValue: nycSchoolDetailsModel?.location ?? "", image: "NS_Location_Image"))
    }
    

    /// Used for updating the store location.
    ///
    /// - Parameter
    /// latitude: Latitude of the selected store.
    /// longitude: longitude of the selected store.
    func setStoreLocation() {
        if let latitude = nycSchoolDetailsModel?.latitude, let longitude = nycSchoolDetailsModel?.longitude {
            mapKit.removeAnnotations(mapKit.annotations)
            let latiD = Double(latitude) ?? 0.0
            let longiD = Double(longitude) ?? 0.0
            let schoolLocation = CLLocationCoordinate2D(latitude: latiD, longitude: longiD)
            let destinationAnnotation = MKPointAnnotation()
            destinationAnnotation.coordinate = schoolLocation
            mapKit.addAnnotation(destinationAnnotation)
            let latitude:CLLocationDegrees = latiD
            let longitude:CLLocationDegrees = longiD

            let location = CLLocationCoordinate2DMake(latitude, longitude)
            let span = MKCoordinateSpanMake(latDelta, lonDelta)
            let region = MKCoordinateRegionMake(location, span)
            mapKit.setRegion(region, animated: true)
        }
    }
}

// MARK: - UITableViewDelegate and UITableViewDataSource
extension NYCSchoolContactDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    
    /// Asks the data source to return the number of sections in the table view.
    ///
    /// - Parameter tableView: An object representing the table view requesting this information.
    /// - Returns: The number of sections in tableView. The default value is 1.
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /// Tells the data source to return the number of rows in a given section of a table view.
    ///
    /// - Parameters:
    ///   - tableView: The table-view object requesting this information.
    ///   - section: An index number identifying a section in tableView.
    /// - Returns: The number of rows in section.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedCellType == SelectedTypeEnum.SatScore ? self.schoolDetailsModel.count : self.schoolContactModel.count
    }
    
    /// Asks the data source for a cell to insert in a particular location of the table view.
    ///
    /// - Parameters:
    ///   - tableView: A table-view object requesting the cell.
    ///   - indexPath: An index path locating a row in tableView.
    /// - Returns: An object inheriting from UITableViewCell that the table view can use for the specified row. An assertion is raised if you return nil.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let detailsCell = tableView.dequeueReusableCell(withIdentifier: String(describing: SchoolInfoTableViewCell.self)) as! SchoolInfoTableViewCell
        detailsCell.selectionStyle = UITableViewCellSelectionStyle.none
        if selectedCellType == SelectedTypeEnum.ContactDetails {
            if self.schoolContactModel.count > 0 {
                detailsCell.subHeaderLabel.text = self.schoolContactModel[indexPath.row].subValue
                detailsCell.headerLabel.text = self.schoolContactModel[indexPath.row].headerValue
                detailsCell.headerIconImageView.image = UIImage(named: self.schoolContactModel[indexPath.row].image ?? "")
            }
        }else {
            if schoolDetailsModel.count > 0 {
                detailsCell.headerLabel.text = schoolDetailsModel[indexPath.row].headerValue
                detailsCell.subHeaderLabel.text = schoolDetailsModel[indexPath.row].subValue
            }
        }
        return detailsCell
    }
}



// MARK: - MKMapViewDelegate Delegate Metgods
extension NYCSchoolContactDetailsViewController: MKMapViewDelegate {
    
    /// Returns the view associated with the specified annotation object.
    ///
    /// - Parameters:
    ///   - mapView: The map view that requested the annotation view.
    ///   - annotation: The object representing the annotation that is about to be displayed. In addition to your custom annotations, this object could be an MKUserLocation object representing the user’s current location.
    /// - Returns: The annotation view to display for the specified annotation or nil if you want to display a standard annotation view.
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseIdentifier = "pinIdentifier"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView?.canShowCallout = true
        } else {
            annotationView?.annotation = annotation
        }
        annotationView?.image = UIImage(named: "NS_Annotation_Image")
        
        return annotationView
    }
}
