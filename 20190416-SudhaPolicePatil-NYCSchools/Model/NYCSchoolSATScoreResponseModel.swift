//
//  NYCSchoolSATScoreResponseModel.swift
//  20190416-SudhaPolicePatil-NYCSchools
//
//  Created by Sudha P on 16/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import Foundation

/// Class used as a response model for SAT Data.
class NYCSchoolSATScoreResponseModel: Serializable {
    var dbn:String?
    var num_of_sat_test_takers:String?
    var sat_critical_reading_avg_score:String?
    var sat_math_avg_score:String?
    var sat_writing_avg_score:String?
    var school_name:String?
}
