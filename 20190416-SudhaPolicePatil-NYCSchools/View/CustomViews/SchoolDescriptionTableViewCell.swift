//
//  SchoolDescriptionTableViewCell.swift
//  20190416-SudhaPolicePatil-NYCSchools
//
//  Created by Sudha P on 16/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import UIKit

/// Cell created to show the description of school.
class SchoolDescriptionTableViewCell: UITableViewCell {
    
    /// Outlet created to show the Activity of the school.
    @IBOutlet weak var activityLabel: UILabel!
    /// Outlet created to show the description on the Activity of the school.
    @IBOutlet weak var descriptionLabel: UILabel!
    
    /// Prepares the receiver for service after it has been loaded from an Interface Builder archive, or nib file.
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    /// Sets the selected state of the cell, optionally animating the transition between states.
    ///
    /// - Parameters:
    ///   - selected: true to set the cell as selected, false to set it as unselected. The default is false.
    ///   - animated: true to animate the transition between selected states, false to make the transition immediate.
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
