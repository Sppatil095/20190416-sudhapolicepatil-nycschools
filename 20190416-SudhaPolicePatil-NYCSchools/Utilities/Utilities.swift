//
//  Utilities.swift
//  APICall
//
//  Created by Sudha P on 16/04/19.
//  Copyright © 2019 Sudha P. All rights reserved.
//

import Foundation
import UIKit

/// Class created for Utility functions.
class Utilities {
    
    /// Method used to get the url based on the key passed.
    ///
    /// - Parameter key: Contain key to fetch the url from plist.
    /// - Returns: Returns the Url from plist based on the key passed.
    class func getURLWith(key: String) -> String? {
        let plistPath: String? = Bundle.main.path(forResource:"URLConfig", ofType: "plist")
        let uriConfigCache: NSDictionary? = plistPath != nil ? NSDictionary(contentsOfFile: plistPath!) : nil
        return uriConfigCache?.value(forKey: key) as? String
    }
}
